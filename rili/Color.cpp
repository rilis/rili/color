#include <algorithm>
#include <cctype>
#include <cmath>
#include <limits>
#include <rili/Color.hpp>
#include <string>
#include <utility>

namespace rili {
namespace color {
namespace {
auto constexpr PI = 3.14159265358979323846;
#define COLOR(X) \
    { #X, CSS::Name::X }

const std::pair<char const* const, CSS::Name> namedColors[] = {COLOR(aliceblue),
                                                               COLOR(antiquewhite),
                                                               COLOR(aqua),
                                                               COLOR(aquamarine),
                                                               COLOR(azure),
                                                               COLOR(beige),
                                                               COLOR(bisque),
                                                               COLOR(black),
                                                               COLOR(blanchedalmond),
                                                               COLOR(blue),
                                                               COLOR(blueviolet),
                                                               COLOR(brown),
                                                               COLOR(burlywood),
                                                               COLOR(cadetblue),
                                                               COLOR(chartreuse),
                                                               COLOR(chocolate),
                                                               COLOR(coral),
                                                               COLOR(cornflowerblue),
                                                               COLOR(cornsilk),
                                                               COLOR(crimson),
                                                               COLOR(cyan),
                                                               COLOR(darkblue),
                                                               COLOR(darkcyan),
                                                               COLOR(darkgoldenrod),
                                                               COLOR(darkgray),
                                                               COLOR(darkgreen),
                                                               COLOR(darkgrey),
                                                               COLOR(darkkhaki),
                                                               COLOR(darkmagenta),
                                                               COLOR(darkolivegreen),
                                                               COLOR(darkorange),
                                                               COLOR(darkorchid),
                                                               COLOR(darkred),
                                                               COLOR(darksalmon),
                                                               COLOR(darkseagreen),
                                                               COLOR(darkslateblue),
                                                               COLOR(darkslategray),
                                                               COLOR(darkslategrey),
                                                               COLOR(darkturquoise),
                                                               COLOR(darkviolet),
                                                               COLOR(deeppink),
                                                               COLOR(deepskyblue),
                                                               COLOR(dimgray),
                                                               COLOR(dimgrey),
                                                               COLOR(dodgerblue),
                                                               COLOR(firebrick),
                                                               COLOR(floralwhite),
                                                               COLOR(forestgreen),
                                                               COLOR(fuchsia),
                                                               COLOR(gainsboro),
                                                               COLOR(ghostwhite),
                                                               COLOR(gold),
                                                               COLOR(goldenrod),
                                                               COLOR(gray),
                                                               COLOR(green),
                                                               COLOR(greenyellow),
                                                               COLOR(grey),
                                                               COLOR(honeydew),
                                                               COLOR(hotpink),
                                                               COLOR(indianred),
                                                               COLOR(indigo),
                                                               COLOR(ivory),
                                                               COLOR(khaki),
                                                               COLOR(lavender),
                                                               COLOR(lavenderblush),
                                                               COLOR(lawngreen),
                                                               COLOR(lemonchiffon),
                                                               COLOR(lightblue),
                                                               COLOR(lightcoral),
                                                               COLOR(lightcyan),
                                                               COLOR(lightgoldenrodyellow),
                                                               COLOR(lightgray),
                                                               COLOR(lightgreen),
                                                               COLOR(lightgrey),
                                                               COLOR(lightpink),
                                                               COLOR(lightsalmon),
                                                               COLOR(lightseagreen),
                                                               COLOR(lightskyblue),
                                                               COLOR(lightslategray),
                                                               COLOR(lightslategrey),
                                                               COLOR(lightsteelblue),
                                                               COLOR(lightyellow),
                                                               COLOR(lime),
                                                               COLOR(limegreen),
                                                               COLOR(linen),
                                                               COLOR(magenta),
                                                               COLOR(maroon),
                                                               COLOR(mediumaquamarine),
                                                               COLOR(mediumblue),
                                                               COLOR(mediumorchid),
                                                               COLOR(mediumpurple),
                                                               COLOR(mediumseagreen),
                                                               COLOR(mediumslateblue),
                                                               COLOR(mediumspringgreen),
                                                               COLOR(mediumturquoise),
                                                               COLOR(mediumvioletred),
                                                               COLOR(midnightblue),
                                                               COLOR(mintcream),
                                                               COLOR(mistyrose),
                                                               COLOR(moccasin),
                                                               COLOR(navajowhite),
                                                               COLOR(navy),
                                                               COLOR(oldlace),
                                                               COLOR(olive),
                                                               COLOR(olivedrab),
                                                               COLOR(orange),
                                                               COLOR(orangered),
                                                               COLOR(orchid),
                                                               COLOR(palegoldenrod),
                                                               COLOR(palegreen),
                                                               COLOR(paleturquoise),
                                                               COLOR(palevioletred),
                                                               COLOR(papayawhip),
                                                               COLOR(peachpuff),
                                                               COLOR(peru),
                                                               COLOR(pink),
                                                               COLOR(plum),
                                                               COLOR(powderblue),
                                                               COLOR(purple),
                                                               COLOR(rebeccapurple),
                                                               COLOR(red),
                                                               COLOR(rosybrown),
                                                               COLOR(royalblue),
                                                               COLOR(saddlebrown),
                                                               COLOR(salmon),
                                                               COLOR(sandybrown),
                                                               COLOR(seagreen),
                                                               COLOR(seashell),
                                                               COLOR(sienna),
                                                               COLOR(silver),
                                                               COLOR(skyblue),
                                                               COLOR(slateblue),
                                                               COLOR(slategray),
                                                               COLOR(slategrey),
                                                               COLOR(snow),
                                                               COLOR(springgreen),
                                                               COLOR(steelblue),
                                                               COLOR(tan),
                                                               COLOR(teal),
                                                               COLOR(thistle),
                                                               COLOR(tomato),
                                                               COLOR(turquoise),
                                                               COLOR(violet),
                                                               COLOR(wheat),
                                                               COLOR(white),
                                                               COLOR(whitesmoke),
                                                               COLOR(yellow),
                                                               COLOR(yellowgreen)};
#undef COLOR
}  // namespace

HSL RGB::hsl() const {
    const double r = static_cast<double>(red) / 255.0;
    const double g = static_cast<double>(green) / 255.0;
    const double b = static_cast<double>(blue) / 255.0;
    const std::uint8_t cmin = std::min({red, green, blue});
    const std::uint8_t cmax = std::max({red, green, blue});
    const double min = std::min({r, g, b});
    const double max = std::max({r, g, b});
    const double delta = max - min;
    double h;
    double s;
    double l;

    if (cmax == cmin) {
        h = 0.0;
    } else if (red == cmax) {
        h = (g - b) / delta;
    } else if (green == cmax) {
        h = 2.0 + (b - r) / delta;
    } else {
        h = 4.0 + (r - g) / delta;
    }

    h = std::min({h * 60.0, 360.0});

    if (h < 0.0) {
        h += 360.0;
    }

    l = (min + max) / 2.0;

    if (cmax == cmin) {
        s = 0.0;
    } else if (l <= 0.5) {
        s = delta / (max + min);
    } else {
        s = delta / (2.0 - max - min);
    }

    return HSL(h, s * 100.0, l * 100.0);
}

HSV RGB::hsv() const {
    const double r = red;
    const double g = green;
    const double b = blue;
    const std::uint8_t cmin = std::min({red, green, blue});
    const std::uint8_t cmax = std::max({red, green, blue});
    const double min = std::min({r, g, b});
    const double max = std::max({r, g, b});
    const double delta = max - min;
    double h;
    double s;
    double v;

    if (cmax == 0) {
        s = 0.0;
    } else {
        s = (delta / max * 1000.0) / 10.0;
    }

    if (cmax == cmin) {
        h = 0.0;
    } else if (red == cmax) {
        h = (g - b) / delta;
    } else if (green == cmax) {
        h = 2.0 + (b - r) / delta;
    } else {
        h = 4.0 + (r - g) / delta;
    }

    h = std::min(h * 60.0, 360.0);

    if (h < 0.0) {
        h += 360.0;
    }

    v = ((max / 255.0) * 1000.0) / 10.0;

    return HSV(h, s, v);
}

HWB RGB::hwb() const {
    const double r = red;
    const double g = green;
    const double b = blue;
    const double h = hsl().hue;
    const double wh = 1.0 * std::min({r, g, b}) / 255.0;
    const double bl = 1.0 - (1.0 * std::max({r, g, b}) / 255.0);
    return HWB(h, wh * 100.0, bl * 100.0);
}

CMYK RGB::cmyk() const {
    const double r = static_cast<double>(red) / 255.0;
    const double g = static_cast<double>(green) / 255.0;
    const double b = static_cast<double>(blue) / 255.0;

    const double k = std::min({1.0 - r, 1.0 - g, 1.0 - b});
    if (k >= 1.0) {
        return CMYK(0, 0, 0, 100.0);
    } else {
        const double c = (1.0 - r - k) / (1.0 - k);
        const double m = (1.0 - g - k) / (1.0 - k);
        const double y = (1.0 - b - k) / (1.0 - k);
        return CMYK(c * 100.0, m * 100.0, y * 100.0, k * 100.0);
    }
}

XYZ RGB::xyz() const {
    double r = static_cast<double>(red) / 255.0;
    double g = static_cast<double>(green) / 255.0;
    double b = static_cast<double>(blue) / 255.0;

    r = r > 0.04045 ? std::pow(((r + 0.055) / 1.055), 2.4) : (r / 12.92);
    g = g > 0.04045 ? std::pow(((g + 0.055) / 1.055), 2.4) : (g / 12.92);
    b = b > 0.04045 ? std::pow(((b + 0.055) / 1.055), 2.4) : (b / 12.92);

    const double x = (r * 0.4124) + (g * 0.3576) + (b * 0.1805);
    const double y = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
    const double z = (r * 0.0193) + (g * 0.1192) + (b * 0.9505);

    return XYZ(x * 100.0, y * 100.0, z * 100.0);
}

LAB RGB::lab() const {
    XYZ xyz_ = xyz();
    double x = xyz_.x;
    double y = xyz_.y;
    double z = xyz_.z;
    double l;
    double a;
    double b;

    x /= 95.047;
    y /= 100.0;
    z /= 108.883;

    constexpr auto c1 = 1.0 / 3.0;
    constexpr auto c2 = 16.0 / 116.0;
    x = x > 0.008856 ? std::pow(x, c1) : (7.787 * x) + c2;
    y = y > 0.008856 ? std::pow(y, c1) : (7.787 * y) + c2;
    z = z > 0.008856 ? std::pow(z, c1) : (7.787 * z) + c2;

    l = (116.0 * y) - 16.0;
    a = 500.0 * (x - y);
    b = 200.0 * (y - z);

    return LAB(l, a, b);
}

ANSI16 RGB::ansi16() const {
    const double value = hsv().value / 50.0;

    if (value < 0.001) {
        return ANSI16(0);
    }

    const auto t1 = static_cast<std::uint32_t>(std::round(blue / 255.0)) << 2u;
    const auto t2 = static_cast<std::uint32_t>(std::round(green / 255.0)) << 1u;
    const auto t3 = static_cast<std::uint32_t>(std::round(red / 255.0));
    auto ansi = t1 | t2 | t3;

    if (ansi == 0 || value > 1.999) {
        ansi += 8;
    }

    return ANSI16(static_cast<std::uint8_t>(ansi));
}

ANSI256 RGB::ansi256() const {
    if (red == green && green == blue) {
        if (red < 8) {
            return ANSI256(16);
        } else if (red > 250) {
            return ANSI256(231);
        } else {
            return ANSI256(static_cast<std::uint8_t>(std::round(((red - 8.0) / 247.0) * 24.0) + 232.0));
        }
    }
    return ANSI256(static_cast<std::uint8_t>(16.0 + (36.0 * std::round(red / 255.0 * 5.0)) +
                                             (6.0 * std::round(green / 255.0 * 5.0)) + std::round(blue / 255.0 * 5.0)));
}

HCG RGB::hcg() const {
    const double r = static_cast<double>(red) / 255.0;
    const double g = static_cast<double>(green) / 255.0;
    const double b = static_cast<double>(blue) / 255.0;
    const auto cmax = std::max({red, green, blue});
    const double max = std::max({r, g, b});
    const double min = std::min({r, g, b});
    const double chroma = max - min;
    double grayscale;
    double hue;

    if (chroma < 1.0) {
        grayscale = min / (1.0 - chroma);
    } else {
        grayscale = 0.0;
    }

    if (chroma <= 0.0) {
        hue = 0.0;
    } else if (cmax == red) {
        hue = std::fmod(((g - b) / chroma), 6);
    } else if (cmax == green) {
        hue = 2 + (b - r) / chroma;
    } else {
        hue = 4 + (r - g) / chroma + 4;
    }

    hue /= 6;
    hue = std::fmod(hue, 1);

    return HCG(hue * 360.0, chroma * 100.0, grayscale * 100.0);
}

GRAY RGB::gray() const {
    return GRAY((static_cast<double>(red) + static_cast<double>(green) + static_cast<double>(blue)) / 3 / 255 * 100);
}

RGB HSL::rgb() const {
    const double h = hue / 360.0;
    const double s = saturation / 100.0;
    const double l = lightness / 100.0;

    constexpr double c1 = 1.0 / 3.0;
    constexpr double c2 = 2.0 / 3.0;
    double val;
    std::uint8_t rgb[3] = {0, 0, 0};

    if (s == 0.0) {
        std::uint8_t v = static_cast<std::uint8_t>(l * 255.0);
        return RGB(v, v, v);
    }

    double t2;
    if (l < 0.5) {
        t2 = l * (1.0 + s);
    } else {
        t2 = l + s - l * s;
    }
    const double t1 = 2.0 * l - t2;
    double t3;
    for (int i = 0; i < 3; i++) {
        t3 = h + c1 * -(i - 1.0);
        if (t3 < 0.0) {
            t3++;
        }
        if (t3 > 1.0) {
            t3--;
        }

        if (6.0 * t3 < 1.0) {
            val = t1 + (t2 - t1) * 6.0 * t3;
        } else if (2.0 * t3 < 1.0) {
            val = t2;
        } else if (3.0 * t3 < 2.0) {
            val = t1 + (t2 - t1) * (c2 - t3) * 6.0;
        } else {
            val = t1;
        }

        rgb[i] = static_cast<std::uint8_t>(val * 255.0);
    }

    return RGB(rgb[0], rgb[1], rgb[2]);
}

ERGB HSL::ergb() const { return rgb().ergb(); }

HSV HSL::hsv() const {
    const double h = hue;
    double s = saturation / 100.0;
    double l = lightness / 100.0;
    const double lmin = std::max(l, 0.01);
    double smin = s;

    l *= 2.0;
    s *= ((l <= 1.0) ? l : 2.0 - l);
    smin *= (lmin <= 1.0 ? lmin : 2.0 - lmin);
    const double v = (l + s) / 2.0;
    const double sv = ((l == 0.0) ? ((2.0 * smin) / (lmin + smin)) : ((2.0 * s) / (l + s)));

    return HSV(h, sv * 100, v * 100);
}

HCG HSL::hcg() const {
    const double s = saturation / 100.0;
    const double l = lightness / 100.0;
    double c = 1;
    double f = 0;

    if (l < 0.5) {
        c = 2.0 * s * l;
    } else {
        c = 2.0 * s * (1.0 - l);
    }

    if (c < 1.0) {
        f = (l - 0.5 * c) / (1.0 - c);
    }

    return HCG(hue, c * 100, f * 100);
}

RGB HSV::rgb() const {
    const double h = hue / 60.0;
    const double s = saturation / 100.0;
    const double v = value / 100.0;
    const int hi = static_cast<int>(std::floor(h)) % 6;

    const double f = h - std::floor(h);
    const double p = 255.0 * v * (1.0 - s);
    const double q = 255.0 * v * (1.0 - (s * f));
    const double t = 255.0 * v * (1.0 - (s * (1.0 - f)));
    const double vt = v * 255.0;

    const std::uint8_t v8 = static_cast<std::uint8_t>(vt);
    const std::uint8_t t8 = static_cast<std::uint8_t>(t);
    const std::uint8_t p8 = static_cast<std::uint8_t>(p);
    const std::uint8_t q8 = static_cast<std::uint8_t>(q);

    switch (hi) {
        case 0:
            return RGB(v8, t8, p8);
        case 1:
            return RGB(q8, v8, p8);
        case 2:
            return RGB(p8, v8, t8);
        case 3:
            return RGB(p8, q8, v8);
        case 4:
            return RGB(t8, p8, v8);
        default:
            return RGB(v8, p8, q8);
    }
}

ERGB HSV::ergb() const { return rgb().ergb(); }

HSL HSV::hsl() const {
    const double h = hue;
    const double s = saturation / 100.0;
    const double v = value / 100.0;
    const double vmin = std::max(v, 0.01);
    double sl;
    double l;

    l = (2.0 - s) * v;
    const double lmin = (2.0 - s) * vmin;
    sl = s * vmin;
    sl /= (lmin <= 1.0) ? lmin : 2.0 - lmin;
    l /= 2.0;

    return HSL(h, sl * 100.0, l * 100.0);
}

ANSI16 HSV::ansi16() const {
    const int v = static_cast<int>(std::round(value / 50.0));

    if (v == 0) {
        return ANSI16(0);
    }
    auto rgb_ = rgb();

    const auto t1 = static_cast<std::uint32_t>(std::round(rgb_.blue / 255.0)) << 2u;
    const auto t2 = static_cast<std::uint32_t>(std::round(rgb_.green / 255.0)) << 1u;
    const auto t3 = static_cast<std::uint32_t>(std::round(rgb_.red / 255.0));
    auto ansi = t1 | t2 | t3;

    if (v == 2) {
        ansi += 8;
    }

    return ANSI16(static_cast<std::uint8_t>(ansi));
}

HCG HSV::hcg() const {
    const double s = saturation / 100.0;
    const double v = value / 100.0;

    double c = s * v;
    double f = 0;

    if (c < 1.0) {
        f = (v - c) / (1 - c);
    }

    return HCG(hue, c * 100, f * 100);
}

RGB HWB::rgb() const {
    const double h = hue / 360.0;
    double wh = whiteness / 100.0;
    double bl = blackness / 100.0;
    const double ratio = wh + bl;

    if (ratio > 1.0) {
        wh /= ratio;
        bl /= ratio;
    }

    const int i = static_cast<int>(std::floor(6.0 * h));
    const double v = 1.0 - bl;
    double f = 6.0 * h - i;

    if ((i & 0x01) != 0) {
        f = 1.0 - f;
    }

    const double n = wh + f * (v - wh);

    double r;
    double g;
    double b;
    switch (i) {
        case 1:
            r = n;
            g = v;
            b = wh;
            break;
        case 2:
            r = wh;
            g = v;
            b = n;
            break;
        case 3:
            r = wh;
            g = n;
            b = v;
            break;
        case 4:
            r = n;
            g = wh;
            b = v;
            break;
        case 5:
            r = v;
            g = wh;
            b = n;
            break;
        case 6:
        case 0:
        default:
            r = v;
            g = n;
            b = wh;
            break;
    }

    return RGB(static_cast<std::uint8_t>(r * 255.0), static_cast<std::uint8_t>(g * 255.0),
               static_cast<std::uint8_t>(b * 255.0));
}

ERGB HWB::ergb() const { return rgb().ergb(); }

HCG HWB::hcg() const {
    const double w = whiteness / 100.0;
    const double b = blackness / 100.0;
    const double v = 1.0 - b;
    const double c = v - w;
    double g = 0.0;

    if (c < 1.0) {
        g = (v - c) / (1.0 - c);
    }

    return HCG(hue, c * 100.0, g * 100.0);
}

RGB CMYK::rgb() const {
    const double c = cyan / 100.0;
    const double m = magenta / 100.0;
    const double y = yellow / 100.0;
    const double k = key / 100.0;

    const double r = 1.0 - std::min(1.0, c * (1.0 - k) + k);
    const double g = 1.0 - std::min(1.0, m * (1.0 - k) + k);
    const double b = 1.0 - std::min(1.0, y * (1.0 - k) + k);

    return RGB(static_cast<std::uint8_t>(r * 255.0), static_cast<std::uint8_t>(g * 255.0),
               static_cast<std::uint8_t>(b * 255.0));
}

ERGB CMYK::ergb() const { return rgb().ergb(); }

RGB XYZ::rgb() const {
    const double x_ = x / 100.0;
    const double y_ = y / 100.0;
    const double z_ = z / 100.0;

    double r = (x_ * 3.2406) + (y_ * -1.5372) + (z_ * -0.4986);
    double g = (x_ * -0.9689) + (y_ * 1.8758) + (z_ * 0.0415);
    double b = (x_ * 0.0557) + (y_ * -0.2040) + (z_ * 1.0570);

    auto constexpr c1 = 1.0 / 2.4;
    r = r > 0.0031308 ? ((1.055 * std::pow(r, c1)) - 0.055) : r * 12.92;
    g = g > 0.0031308 ? ((1.055 * std::pow(g, c1)) - 0.055) : g * 12.92;
    b = b > 0.0031308 ? ((1.055 * std::pow(b, c1)) - 0.055) : b * 12.92;

    r = std::min(std::max(0.0, r), 1.0);
    g = std::min(std::max(0.0, g), 1.0);
    b = std::min(std::max(0.0, b), 1.0);

    return RGB(static_cast<std::uint8_t>(r * 255.0), static_cast<std::uint8_t>(g * 255.0),
               static_cast<std::uint8_t>(b * 255.0));
}

ERGB XYZ::ergb() const { return rgb().ergb(); }

LAB XYZ::lab() const {
    double x_ = x / 95.047;
    double y_ = y / 100.0;
    double z_ = z / 108.883;
    constexpr auto c1 = 1.0 / 3.0;
    constexpr auto c2 = 16.0 / 116.0;

    x_ = (x_ > 0.008856 ? std::pow(x_, c1) : (7.787 * x_) + c2);
    y_ = (y_ > 0.008856 ? std::pow(y_, c1) : (7.787 * y_) + c2);
    z_ = (z_ > 0.008856 ? std::pow(z_, c1) : (7.787 * z_) + c2);

    const double l = (116.0 * y_) - 16.0;
    const double a = 500.0 * (x_ - y_);
    const double b = 200.0 * (y_ - z_);

    return LAB(l, a, b);
}

XYZ LAB::xyz() const {
    double y = (lightness + 16.0) / 116.0;
    double x = a / 500.0 + y;
    double z = y - b / 200.0;

    const double y2 = std::pow(y, 3.0);
    const double x2 = std::pow(x, 3.0);
    const double z2 = std::pow(z, 3.0);
    y = y2 > 0.008856 ? y2 : (y - 16 / 116) / 7.787;
    x = x2 > 0.008856 ? x2 : (x - 16 / 116) / 7.787;
    z = z2 > 0.008856 ? z2 : (z - 16 / 116) / 7.787;

    x *= 95.047;
    y *= 100.0;
    z *= 108.883;

    return XYZ(x, y, z);
}

LCH LAB::lch() const {
    const double hr = std::atan2(b, a);
    const double c = std::sqrt(a * a + b * b);
    double h = hr * 360.0 / 2.0 / PI;
    if (h < 0) {
        h += 360;
    }
    return LCH(lightness, c, h);
}

LAB LCH::lab() const {
    const double hr = hue / 360.0 * 2.0 * PI;
    const double a = chroma * std::cos(hr);
    const double b = chroma * std::sin(hr);

    return LAB(lightness, a, b);
}

RGB HCG::rgb() const {
    const double h = hue / 360.0;
    const double c = chroma / 100.0;
    const double g = grayness / 100.0;

    if (c == 0.0) {
        auto t1 = static_cast<std::uint8_t>(g * 255.0);
        return RGB(t1, t1, t1);
    }

    double pure[3] = {0.0, 0.0, 0.0};
    const double hi = std::fmod(h, 1.0) * 6.0;
    const double v = std::fmod(hi, 1.0);
    const double w = 1.0 - v;

    switch (static_cast<std::uint8_t>(std::floor(hi))) {
        case 0:
            pure[0] = 1.0;
            pure[1] = v;
            pure[2] = 0.0;
            break;
        case 1:
            pure[0] = w;
            pure[1] = 1.0;
            pure[2] = 0.0;
            break;
        case 2:
            pure[0] = 0.0;
            pure[1] = 1.0;
            pure[2] = v;
            break;
        case 3:
            pure[0] = 0.0;
            pure[1] = w;
            pure[2] = 1.0;
            break;
        case 4:
            pure[0] = v;
            pure[1] = 0.0;
            pure[2] = 1.0;
            break;
        default:
            pure[0] = 1.0;
            pure[1] = 0.0;
            pure[2] = w;
    }

    const double mg = (1.0 - c) * g;

    return RGB(static_cast<std::uint8_t>((c * pure[0] + mg) * 255.0),
               static_cast<std::uint8_t>((c * pure[1] + mg) * 255.0),
               static_cast<std::uint8_t>((c * pure[2] + mg) * 255.0));
}

ERGB HCG::ergb() const { return rgb().ergb(); }

HSL HCG::hsl() const {
    const double c = chroma / 100.0;
    const double g = grayness / 100.0;

    const double l = g * (1.0 - c) + 0.5 * c;
    double s = 0.0;

    if (l > 0.0 && l < 0.5) {
        s = c / (2.0 * l);
    } else if (l >= 0.5 && l < 1.0) {
        s = c / (2.0 * (1.0 - l));
    }

    return HSL(hue, s * 100.0, l * 100.0);
}

HSV HCG::hsv() const {
    const double c = chroma / 100.0;
    const double g = grayness / 100.0;

    const double v = c + g * (1.0 - c);
    double f = 0.0;

    if (v > 0.0) {
        f = c / v;
    }

    return HSV(hue, f * 100.0, v * 100.0);
}

HWB HCG::hwb() const {
    const double c = chroma / 100.0;
    const double g = grayness / 100.0;
    const double v = c + g * (1.0 - c);
    return HWB(hue, (v - c) * 100.0, (1 - v) * 100.0);
}

RGB ANSI16::rgb() const {
    switch (value) {
        case 0:
            return RGB(0, 0, 0);
        case 1:
            return RGB(205, 0, 0);
        case 2:
            return RGB(0, 205, 0);
        case 3:
            return RGB(205, 205, 0);
        case 4:
            return RGB(0, 0, 205);
        case 5:
            return RGB(205, 0, 205);
        case 6:
            return RGB(0, 205, 205);
        case 7:
            return RGB(205, 205, 205);
        case 8:
            return RGB(127, 127, 127);
        case 9:
            return RGB(255, 0, 0);
        case 10:
            return RGB(0, 255, 0);
        case 11:
            return RGB(255, 255, 0);
        case 12:
            return RGB(0, 0, 255);
        case 13:
            return RGB(255, 0, 255);
        case 14:
            return RGB(0, 255, 255);
        case 15:
        default:
            return RGB(255, 255, 255);
    }
}

ERGB ANSI16::ergb() const { return rgb().ergb(); }

RGB ANSI256::rgb() const {
    if (value >= 232) {
        std::uint8_t c = static_cast<std::uint8_t>((value - 232) * 10.5 + 8);
        return RGB(c, c, c);
    }

    if (value >= 16) {
        std::uint8_t b = static_cast<std::uint8_t>(value - 16);
        std::uint8_t r = b / 36;
        if (r > 5) {
            r = 5;
        }
        b = static_cast<std::uint8_t>(b - r * 36);
        std::uint8_t g = b / 6;
        if (g > 5) {
            g = 5;
        }
        b = static_cast<std::uint8_t>(b - g * 6);
        return RGB(static_cast<std::uint8_t>(r * 51), static_cast<std::uint8_t>(g * 51),
                   static_cast<std::uint8_t>(b * 51));
    } else {
        return ANSI16(value).rgb();
    }
}

ERGB ANSI256::ergb() const { return rgb().ergb(); }
ANSI16 ANSI256::ansi16() const {
    if (value < 16) {
        return ANSI16(value);
    } else {
        return rgb().ansi16();
    }
}

RGB GRAY::rgb() const {
    const auto c = static_cast<std::uint8_t>(value / 100.0 * 255.0);
    return RGB(c, c, c);
}

ERGB GRAY::ergb() const { return rgb().ergb(); }

RGB CSS::rgb() const {
    const std::uint32_t v = static_cast<std::uint32_t>(name);
    const std::uint8_t r = static_cast<std::uint8_t>((v & 0x00FF0000) >> 16);
    const std::uint8_t g = static_cast<std::uint8_t>((v & 0x0000FF00) >> 8);
    const std::uint8_t b = static_cast<std::uint8_t>((v & 0x000000FF));
    return RGB(r, g, b);
}

ERGB CSS::ergb() const { return rgb().ergb(); }

std::string CSS::string() const {
    auto found = std::find_if(std::begin(namedColors), std::end(namedColors),
                              [this](std::pair<char const* const, Name> const& c) { return c.second == name; });
    if (found != std::end(namedColors)) {
        return found->first;
    } else {
        return "pink";
    }
}

CSS::CSS(const std::string& n) : Color(), name(Name::pink) {
    auto nn = n;
    for (auto& c : nn) {
        if (c >= 'A' && c <= 'Z') {
            c = static_cast<char>(c - 'A' + 'a');
        }
    }
    auto found = std::find_if(std::begin(namedColors), std::end(namedColors),
                              [&n](std::pair<char const* const, Name> const& c) { return c.first == n; });
    if (found != std::end(namedColors)) {
        name = found->second;
    }
}

CSS RGB::css() const {
    const auto distance = [this](RGB other) {
        return static_cast<double>(red) * static_cast<double>(other.red) +
               static_cast<double>(blue) * static_cast<double>(other.blue) +
               static_cast<double>(green) * static_cast<double>(other.green);
    };
    double dist = std::numeric_limits<double>::max();
    CSS::Name n = CSS::Name::black;
    double currentDist;
    for (std::pair<char const* const, CSS::Name> const& c : namedColors) {
        currentDist = distance(CSS(c.second).rgb());
        if (dist > currentDist) {
            dist = currentDist;
            n = c.second;
        }
    }
    return CSS(n);
}

RGB RGB::rgb() const { return *this; }

ERGB RGB::ergb() const {
    return ERGB(static_cast<double>(red) / 255.0, static_cast<double>(green) / 255.0,
                static_cast<double>(blue) / 255.0);
}
LCH RGB::lch() const { return lab().lch(); }
ANSI256 HSV::ansi256() const { return rgb().ansi256(); }
GRAY HSV::gray() const { return rgb().gray(); }
CSS HSV::css() const { return rgb().css(); }
HSL HWB::hsl() const { return rgb().hsl(); }
HSV HWB::hsv() const { return rgb().hsv(); }
HWB HWB::hwb() const { return *this; }
CMYK HWB::cmyk() const { return rgb().cmyk(); }
XYZ HWB::xyz() const { return rgb().xyz(); }
LAB HWB::lab() const { return rgb().lab(); }
LCH HWB::lch() const { return rgb().lch(); }
ANSI16 HWB::ansi16() const { return rgb().ansi16(); }
ANSI256 HWB::ansi256() const { return rgb().ansi256(); }
GRAY HWB::gray() const { return rgb().gray(); }
CSS HWB::css() const { return rgb().css(); }
HWB HSL::hwb() const { return hcg().hwb(); }
CMYK HSL::cmyk() const { return rgb().cmyk(); }
XYZ HSL::xyz() const { return rgb().xyz(); }
LAB HSL::lab() const { return rgb().lab(); }
LCH HSL::lch() const { return rgb().lch(); }
ANSI16 HSL::ansi16() const { return hsv().ansi16(); }
ANSI256 HSL::ansi256() const { return rgb().ansi256(); }
GRAY HSL::gray() const { return rgb().gray(); }
HSV HSV::hsv() const { return *this; }
HWB HSV::hwb() const { return hcg().hwb(); }
CMYK HSV::cmyk() const { return rgb().cmyk(); }
XYZ HSV::xyz() const { return rgb().xyz(); }
LAB HSV::lab() const { return rgb().lab(); }
LCH HSV::lch() const { return rgb().lch(); }
CSS HSL::css() const { return rgb().css(); }
HSL HSL::hsl() const { return *this; }
HSL CMYK::hsl() const { return rgb().hsl(); }
HSV CMYK::hsv() const { return rgb().hsv(); }
HWB CMYK::hwb() const { return rgb().hwb(); }
CMYK CMYK::cmyk() const { return *this; }
XYZ CMYK::xyz() const { return rgb().xyz(); }
LAB CMYK::lab() const { return rgb().lab(); }
LCH CMYK::lch() const { return rgb().lch(); }
ANSI16 CMYK::ansi16() const { return rgb().ansi16(); }
ANSI256 CMYK::ansi256() const { return rgb().ansi256(); }
HCG CMYK::hcg() const { return rgb().hcg(); }
GRAY CMYK::gray() const { return rgb().gray(); }
CSS CMYK::css() const { return rgb().css(); }
HSL XYZ::hsl() const { return rgb().hsl(); }
HSV XYZ::hsv() const { return rgb().hsv(); }
HWB XYZ::hwb() const { return rgb().hwb(); }
CMYK XYZ::cmyk() const { return rgb().cmyk(); }
XYZ XYZ::xyz() const { return *this; }
ANSI16 LAB::ansi16() const { return rgb().ansi16(); }
ANSI256 LAB::ansi256() const { return rgb().ansi256(); }
HCG LAB::hcg() const { return rgb().hcg(); }
GRAY LAB::gray() const { return rgb().gray(); }
CSS LAB::css() const { return rgb().css(); }
RGB LCH::rgb() const { return lab().rgb(); }
ERGB LCH::ergb() const { return rgb().ergb(); }
HSL LCH::hsl() const { return rgb().hsl(); }
HSV LCH::hsv() const { return rgb().hsv(); }
HWB LCH::hwb() const { return rgb().hwb(); }
CMYK LCH::cmyk() const { return rgb().cmyk(); }
XYZ LCH::xyz() const { return lab().xyz(); }
LAB LAB::lab() const { return *this; }
LCH XYZ::lch() const { return lab().lch(); }
ANSI16 XYZ::ansi16() const { return rgb().ansi16(); }
ANSI256 XYZ::ansi256() const { return rgb().ansi256(); }
HCG XYZ::hcg() const { return rgb().hcg(); }
GRAY XYZ::gray() const { return rgb().gray(); }
CSS XYZ::css() const { return rgb().css(); }
RGB LAB::rgb() const { return xyz().rgb(); }
ERGB LAB::ergb() const { return rgb().ergb(); }
HSL LAB::hsl() const { return rgb().hsl(); }
HSV LAB::hsv() const { return rgb().hsv(); }
HWB LAB::hwb() const { return rgb().hwb(); }
CMYK LAB::cmyk() const { return rgb().cmyk(); }
LCH LCH::lch() const { return *this; }
ANSI16 LCH::ansi16() const { return rgb().ansi16(); }
ANSI256 LCH::ansi256() const { return rgb().ansi256(); }
HCG LCH::hcg() const { return rgb().hcg(); }
GRAY LCH::gray() const { return rgb().gray(); }
CSS LCH::css() const { return rgb().css(); }
CMYK HCG::cmyk() const { return rgb().cmyk(); }
XYZ HCG::xyz() const { return rgb().xyz(); }
LAB HCG::lab() const { return rgb().lab(); }
LCH HCG::lch() const { return rgb().lch(); }
ANSI16 HCG::ansi16() const { return rgb().ansi16(); }
ANSI256 HCG::ansi256() const { return rgb().ansi256(); }
HCG HCG::hcg() const { return *this; }
GRAY HCG::gray() const { return rgb().gray(); }
CSS HCG::css() const { return rgb().css(); }
HSL ANSI16::hsl() const { return rgb().hsl(); }
HSV ANSI16::hsv() const { return rgb().hsv(); }
HWB ANSI16::hwb() const { return rgb().hwb(); }
CMYK ANSI16::cmyk() const { return rgb().cmyk(); }
XYZ ANSI16::xyz() const { return rgb().xyz(); }
LAB ANSI16::lab() const { return rgb().lab(); }
LCH ANSI16::lch() const { return rgb().lch(); }
ANSI16 ANSI16::ansi16() const { return *this; }
ANSI256 ANSI16::ansi256() const { return ANSI256(value); }
HCG ANSI16::hcg() const { return rgb().hcg(); }
GRAY ANSI16::gray() const { return rgb().gray(); }
CSS ANSI16::css() const { return rgb().css(); }
HSL ANSI256::hsl() const { return rgb().hsl(); }
HSV ANSI256::hsv() const { return rgb().hsv(); }
HWB ANSI256::hwb() const { return rgb().hwb(); }
CMYK ANSI256::cmyk() const { return rgb().cmyk(); }
XYZ ANSI256::xyz() const { return rgb().xyz(); }
LAB ANSI256::lab() const { return rgb().lab(); }
LCH ANSI256::lch() const { return rgb().lch(); }
ANSI256 ANSI256::ansi256() const { return *this; }
HCG ANSI256::hcg() const { return rgb().hcg(); }
GRAY ANSI256::gray() const { return rgb().gray(); }
CSS ANSI256::css() const { return rgb().css(); }
HSL GRAY::hsl() const { return HSL(0, 0, value); }
HSV GRAY::hsv() const { return hsl().hsv(); }
HWB GRAY::hwb() const { return HWB(0, 100, value); }
CMYK GRAY::cmyk() const { return CMYK(0, 0, 0, value); }
XYZ GRAY::xyz() const { return lab().xyz(); }
LAB GRAY::lab() const { return LAB(value, 0, 0); }
LCH GRAY::lch() const { return lab().lch(); }
ANSI16 GRAY::ansi16() const { return rgb().ansi16(); }
ANSI256 GRAY::ansi256() const { return rgb().ansi256(); }
HCG GRAY::hcg() const { return hwb().hcg(); }
GRAY GRAY::gray() const { return *this; }
CSS GRAY::css() const { return rgb().css(); }
HSL CSS::hsl() const { return rgb().hsl(); }
HSV CSS::hsv() const { return rgb().hsv(); }
HWB CSS::hwb() const { return rgb().hwb(); }
CMYK CSS::cmyk() const { return rgb().cmyk(); }
XYZ CSS::xyz() const { return rgb().xyz(); }
LAB CSS::lab() const { return rgb().lab(); }
LCH CSS::lch() const { return rgb().lch(); }
ANSI16 CSS::ansi16() const { return rgb().ansi16(); }
ANSI256 CSS::ansi256() const { return rgb().ansi256(); }
HCG CSS::hcg() const { return rgb().hcg(); }
GRAY CSS::gray() const { return rgb().gray(); }
CSS CSS::css() const { return *this; }
RGB ERGB::rgb() const {
    return RGB(std::uint8_t(::round(red * 255)), std::uint8_t(::round(green * 255)), std::uint8_t(::round(blue * 255)));
}
ERGB ERGB::ergb() const { return *this; }
HSL ERGB::hsl() const { return rgb().hsl(); }
HSV ERGB::hsv() const { return rgb().hsv(); }
HWB ERGB::hwb() const { return rgb().hwb(); }
CMYK ERGB::cmyk() const { return rgb().cmyk(); }
XYZ ERGB::xyz() const { return rgb().xyz(); }
LAB ERGB::lab() const { return rgb().lab(); }
LCH ERGB::lch() const { return rgb().lch(); }
ANSI16 ERGB::ansi16() const { return rgb().ansi16(); }
ANSI256 ERGB::ansi256() const { return rgb().ansi256(); }
HCG ERGB::hcg() const { return rgb().hcg(); }
GRAY ERGB::gray() const { return rgb().gray(); }
CSS ERGB::css() const { return rgb().css(); }

}  // namespace color
}  // namespace rili
