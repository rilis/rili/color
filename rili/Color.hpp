#pragma once
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <string>

namespace rili {
namespace color {
class RGB;
class HSL;
class HSV;
class HWB;
class CMYK;
class XYZ;
class LAB;
class LCH;
class ANSI16;
class ANSI256;
class HCG;
class GRAY;
class CSS;
class ERGB;
}  // namespace color

/**
 * @brief The Color class is base for all color models
 */
class Color {
 public:
    virtual ~Color() = default;

 protected:
    /**
     * @brief Color
     * @param other
     */
    Color(Color const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    Color& operator=(Color const& other) = default;
    Color() = default;

 public:
    /**
     * @brief ergb convert color in current model to ergb
     * @return
     */
    virtual color::ERGB ergb() const = 0;
    /**
     * @brief rgb convert color in current model to rgb
     * @return
     */
    virtual color::RGB rgb() const = 0;
    /**
     * @brief hsl convert color in current model to hsl
     * @return
     */
    virtual color::HSL hsl() const = 0;
    /**
     * @brief hsv convert color in current model to hsv
     * @return
     */
    virtual color::HSV hsv() const = 0;
    /**
     * @brief hwb convert color in current model to hwb
     * @return
     */
    virtual color::HWB hwb() const = 0;
    /**
     * @brief cmyk convert color in current model to cmyk
     * @return
     */
    virtual color::CMYK cmyk() const = 0;
    /**
     * @brief xyz convert color in current model to xyz
     * @return
     */
    virtual color::XYZ xyz() const = 0;
    /**
     * @brief lab convert color in current model to lab
     * @return
     */
    virtual color::LAB lab() const = 0;
    /**
     * @brief lch convert color in current model to lch
     * @return
     */
    virtual color::LCH lch() const = 0;
    /**
     * @brief ansi16 convert color in current model to ansi 16 color palete
     * @return
     */
    virtual color::ANSI16 ansi16() const = 0;
    /**
     * @brief ansi256 convert color in current model to ansi 256 color palete
     * @return
     */
    virtual color::ANSI256 ansi256() const = 0;
    /**
     * @brief hcg convert color in current model to hcg
     * @return
     */
    virtual color::HCG hcg() const = 0;
    /**
     * @brief gray convert color in current model to gray scale
     * @return
     */
    virtual color::GRAY gray() const = 0;
    /**
     * @brief gray convert color in current model to css named color
     * @return
     */
    virtual color::CSS css() const = 0;
};

namespace color {

/**
 * @brief The RGB class
 */
class RGB : public Color {
 public:
    /**
     * @brief RGB
     * @param other
     */
    RGB(RGB const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    RGB& operator=(RGB const& other) = default;

 public:
    virtual ~RGB() = default;
    RGB() = delete;
    /**
     * @brief RGB
     * @param r
     * @param g
     * @param b
     */
    inline RGB(std::uint8_t r, std::uint8_t g, std::uint8_t b) : Color(), red(r), green(g), blue(b) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief red
     */
    std::uint8_t red;
    /**
     * @brief green
     */
    std::uint8_t green;
    /**
     * @brief blue
     */
    std::uint8_t blue;
};

/**
 * @brief The ERGB class represent RGB color where storage is extended to normalized [0.0 - 1.0] double for each
 * elementary color
 */
class ERGB : public Color {
 public:
    /**
     * @brief ERGB
     * @param other
     */
    ERGB(ERGB const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    ERGB& operator=(ERGB const& other) = default;

 public:
    virtual ~ERGB() = default;
    ERGB() = delete;
    /**
     * @brief RGB
     * @param r
     * @param g
     * @param b
     */
    inline ERGB(double r, double g, double b) : Color(), red(r), green(g), blue(b) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief red
     */
    double red;
    /**
     * @brief green
     */
    double green;
    /**
     * @brief blue
     */
    double blue;
};

/**
 * @brief The CSS class
 */
class CSS : public Color {
 public:
    /**
     * @brief CSS
     * @param other
     */
    CSS(CSS const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    CSS& operator=(CSS const& other) = default;

 public:
    virtual ~CSS() = default;
    CSS() = delete;
    /**
     * @brief The Name enum represent id for known CSS color names with they value as rgb hex number.
     *
     * @note Not all of these enums have unique values.
     */
    enum class Name {
        aliceblue = 0xf0f8ff,
        antiquewhite = 0xfaebd7,
        aqua = 0x00ffff,
        aquamarine = 0x7fffd4,
        azure = 0xf0ffff,
        beige = 0xf5f5dc,
        bisque = 0xffe4c4,
        black = 0x000000,
        blanchedalmond = 0xffebcd,
        blue = 0x0000ff,
        blueviolet = 0x8a2be2,
        brown = 0xa52a2a,
        burlywood = 0xdeb887,
        cadetblue = 0x5f9ea0,
        chartreuse = 0x7fff00,
        chocolate = 0xd2691e,
        coral = 0xff7f50,
        cornflowerblue = 0x6495ed,
        cornsilk = 0xfff8dc,
        crimson = 0xdc143c,
        cyan = 0x00ffff,
        darkblue = 0x00008b,
        darkcyan = 0x008b8b,
        darkgoldenrod = 0xb8860b,
        darkgray = 0xa9a9a9,
        darkgreen = 0x006400,
        darkgrey = 0xa9a9a9,
        darkkhaki = 0xbdb76b,
        darkmagenta = 0x8b008b,
        darkolivegreen = 0x556b2f,
        darkorange = 0xff8c00,
        darkorchid = 0x9932cc,
        darkred = 0x8b0000,
        darksalmon = 0xe9967a,
        darkseagreen = 0x8fbc8f,
        darkslateblue = 0x483d8b,
        darkslategray = 0x2f4f4f,
        darkslategrey = 0x2f4f4f,
        darkturquoise = 0x00ced1,
        darkviolet = 0x9400d3,
        deeppink = 0xff1493,
        deepskyblue = 0x00bfff,
        dimgray = 0x696969,
        dimgrey = 0x696969,
        dodgerblue = 0x1e90ff,
        firebrick = 0xb22222,
        floralwhite = 0xfffaf0,
        forestgreen = 0x228b22,
        fuchsia = 0xff00ff,
        gainsboro = 0xdcdcdc,
        ghostwhite = 0xf8f8ff,
        gold = 0xffd700,
        goldenrod = 0xdaa520,
        gray = 0x808080,
        green = 0x008000,
        greenyellow = 0xadff2f,
        grey = 0x808080,
        honeydew = 0xf0fff0,
        hotpink = 0xff69b4,
        indianred = 0xcd5c5c,
        indigo = 0x4b0082,
        ivory = 0xfffff0,
        khaki = 0xf0e68c,
        lavender = 0xe6e6fa,
        lavenderblush = 0xfff0f5,
        lawngreen = 0x7cfc00,
        lemonchiffon = 0xfffacd,
        lightblue = 0xadd8e6,
        lightcoral = 0xf08080,
        lightcyan = 0xe0ffff,
        lightgoldenrodyellow = 0xfafad2,
        lightgray = 0xd3d3d3,
        lightgreen = 0x90ee90,
        lightgrey = 0xd3d3d3,
        lightpink = 0xffb6c1,
        lightsalmon = 0xffa07a,
        lightseagreen = 0x20b2aa,
        lightskyblue = 0x87cefa,
        lightslategray = 0x778899,
        lightslategrey = 0x778899,
        lightsteelblue = 0xb0c4de,
        lightyellow = 0xffffe0,
        lime = 0x00ff00,
        limegreen = 0x32cd32,
        linen = 0xfaf0e6,
        magenta = 0xff00ff,
        maroon = 0x800000,
        mediumaquamarine = 0x66cdaa,
        mediumblue = 0x0000cd,
        mediumorchid = 0xba55d3,
        mediumpurple = 0x9370db,
        mediumseagreen = 0x3cb371,
        mediumslateblue = 0x7b68ee,
        mediumspringgreen = 0x00fa9a,
        mediumturquoise = 0x48d1cc,
        mediumvioletred = 0xc71585,
        midnightblue = 0x191970,
        mintcream = 0xf5fffa,
        mistyrose = 0xffe4e1,
        moccasin = 0xffe4b5,
        navajowhite = 0xffdead,
        navy = 0x000080,
        oldlace = 0xfdf5e6,
        olive = 0x808000,
        olivedrab = 0x6b8e23,
        orange = 0xffa500,
        orangered = 0xff4500,
        orchid = 0xda70d6,
        palegoldenrod = 0xeee8aa,
        palegreen = 0x98fb98,
        paleturquoise = 0xafeeee,
        palevioletred = 0xdb7093,
        papayawhip = 0xffefd5,
        peachpuff = 0xffdab9,
        peru = 0xcd853f,
        pink = 0xffc0cb,
        plum = 0xdda0dd,
        powderblue = 0xb0e0e6,
        purple = 0x800080,
        rebeccapurple = 0x663399,
        red = 0xff0000,
        rosybrown = 0xbc8f8f,
        royalblue = 0x4169e1,
        saddlebrown = 0x8b4513,
        salmon = 0xfa8072,
        sandybrown = 0xf4a460,
        seagreen = 0x2e8b57,
        seashell = 0xfff5ee,
        sienna = 0xa0522d,
        silver = 0xc0c0c0,
        skyblue = 0x87ceeb,
        slateblue = 0x6a5acd,
        slategray = 0x708090,
        slategrey = 0x708090,
        snow = 0xfffafa,
        springgreen = 0x00ff7f,
        steelblue = 0x4682b4,
        tan = 0xd2b48c,
        teal = 0x008080,
        thistle = 0xd8bfd8,
        tomato = 0xff6347,
        turquoise = 0x40e0d0,
        violet = 0xee82ee,
        wheat = 0xf5deb3,
        white = 0xffffff,
        whitesmoke = 0xf5f5f5,
        yellow = 0xffff00,
        yellowgreen
    };
    /**
     * @brief CSS construct color from string with name matching to identifier in case non sensitive way. If color name
     * not known it defaults to pink.
     * @param n
     */
    explicit CSS(std::string const& n);

    /**
     * @brief CSS construct color from it identifier
     * @param n - ientifier
     */
    explicit inline CSS(Name n) : Color(), name(n) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    /**
     * @brief string convert color to css named color string representation
     * @return
     */
    std::string string() const;
    CSS css() const override;

 public:
    /**
     * @brief name
     */
    Name name;
};

/**
 * @brief The HSL class
 */
class HSL : public Color {
 public:
    /**
     * @brief HSL
     * @param other
     */
    HSL(HSL const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    HSL& operator=(HSL const& other) = default;

 public:
    virtual ~HSL() = default;
    HSL() = delete;
    /**
     * @brief HSL
     * @param h
     * @param s
     * @param l
     */
    inline HSL(double h, double s, double l) : Color(), hue(h), saturation(s), lightness(l) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief hue
     */
    double hue;
    /**
     * @brief saturation
     */
    double saturation;
    /**
     * @brief lightness
     */
    double lightness;
};

/**
 * @brief The HSV class
 */
class HSV : public Color {
 public:
    /**
     * @brief HSV
     * @param other
     */
    HSV(HSV const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    HSV& operator=(HSV const& other) = default;

 public:
    virtual ~HSV() = default;
    HSV() = delete;
    /**
     * @brief HSV
     * @param h
     * @param s
     * @param v
     */
    inline HSV(double h, double s, double v) : Color(), hue(h), saturation(s), value(v) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief hue
     */
    double hue;
    /**
     * @brief saturation
     */
    double saturation;
    /**
     * @brief value
     */
    double value;
};

/**
 * @brief The HWB class
 */
class HWB : public Color {
 public:
    /**
     * @brief HWB
     * @param other
     */
    HWB(HWB const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    HWB& operator=(HWB const& other) = default;

 public:
    virtual ~HWB() = default;
    HWB() = delete;
    /**
     * @brief HWB
     * @param h
     * @param w
     * @param b
     */
    inline HWB(double h, double w, double b) : Color(), hue(h), whiteness(w), blackness(b) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief hue
     */
    double hue;
    /**
     * @brief whiteness
     */
    double whiteness;
    /**
     * @brief blackness
     */
    double blackness;
};

/**
 * @brief The CMYK class
 */
class CMYK : public Color {
 public:
    /**
     * @brief CMYK
     * @param other
     */
    CMYK(CMYK const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    CMYK& operator=(CMYK const& other) = default;

 public:
    virtual ~CMYK() = default;
    CMYK() = delete;
    /**
     * @brief CMYK
     * @param c
     * @param m
     * @param y
     * @param k
     */
    inline CMYK(double c, double m, double y, double k) : Color(), cyan(c), magenta(m), yellow(y), key(k) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief cyan
     */
    double cyan;
    /**
     * @brief magenta
     */
    double magenta;
    /**
     * @brief yellow
     */
    double yellow;
    /**
     * @brief key
     */
    double key;
};

/**
 * @brief The XYZ class
 */
class XYZ : public Color {
 public:
    /**
     * @brief XYZ
     * @param other
     */
    XYZ(XYZ const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    XYZ& operator=(XYZ const& other) = default;

 public:
    virtual ~XYZ() = default;
    XYZ() = delete;
    /**
     * @brief XYZ
     * @param x
     * @param y
     * @param z
     */
    inline XYZ(double x, double y, double z) : Color(), x(x), y(y), z(z) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief x
     */
    double x;
    /**
     * @brief y
     */
    double y;
    /**
     * @brief z
     */
    double z;
};

/**
 * @brief The LAB class
 */
class LAB : public Color {
 public:
    /**
     * @brief LAB
     * @param other
     */
    LAB(LAB const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    LAB& operator=(LAB const& other) = default;

 public:
    virtual ~LAB() = default;
    LAB() = delete;
    /**
     * @brief LAB
     * @param l
     * @param a
     * @param b
     */
    inline LAB(double l, double a, double b) : Color(), lightness(l), a(a), b(b) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief lightness
     */
    double lightness;
    /**
     * @brief a
     */
    double a;
    /**
     * @brief b
     */
    double b;
};

/**
 * @brief The LCH class
 */
class LCH : public Color {
 public:
    /**
     * @brief LCH
     * @param other
     */
    LCH(LCH const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    LCH& operator=(LCH const& other) = default;

 public:
    virtual ~LCH() = default;
    LCH() = delete;
    /**
     * @brief LCH
     * @param l
     * @param c
     * @param h
     */
    inline LCH(double l, double c, double h) : Color(), lightness(l), chroma(c), hue(h) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief lightness
     */
    double lightness;
    /**
     * @brief chroma
     */
    double chroma;
    /**
     * @brief hue
     */
    double hue;
};

/**
 * @brief The HCG class
 */
class HCG : public Color {
 public:
    /**
     * @brief HCG
     * @param other
     */
    HCG(HCG const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    HCG& operator=(HCG const& other) = default;

 public:
    virtual ~HCG() = default;
    HCG() = delete;
    /**
     * @brief HCG
     * @param h
     * @param c
     * @param g
     */
    inline HCG(double h, double c, double g) : Color(), hue(h), chroma(c), grayness(g) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief hue
     */
    double hue;
    /**
     * @brief chroma
     */
    double chroma;
    /**
     * @brief grayness
     */
    double grayness;
};

/**
 * @brief The ANSI16 class
 */
class ANSI16 : public Color {
 public:
    /**
     * @brief ANSI16
     * @param other
     */
    ANSI16(ANSI16 const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    ANSI16& operator=(ANSI16 const& other) = default;

 public:
    virtual ~ANSI16() = default;
    ANSI16() = delete;
    /**
     * @brief ANSI16
     * @param v
     */
    explicit inline ANSI16(std::uint8_t v) : Color(), value(v) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief value
     */
    std::uint8_t value;
};

/**
 * @brief The ANSI256 class
 */
class ANSI256 : public Color {
 public:
    /**
     * @brief ANSI256
     * @param other
     */
    ANSI256(ANSI256 const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    ANSI256& operator=(ANSI256 const& other) = default;

 public:
    virtual ~ANSI256() = default;
    ANSI256() = delete;
    /**
     * @brief ANSI256
     * @param v
     */
    explicit inline ANSI256(std::uint8_t v) : Color(), value(v) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief value
     */
    std::uint8_t value;
};

/**
 * @brief The GRAY class
 */
class GRAY : public Color {
 public:
    /**
     * @brief GRAY
     * @param other
     */
    GRAY(GRAY const& other) = default;
    /**
     * @brief operator=
     * @param other
     * @return
     */
    GRAY& operator=(GRAY const& other) = default;

 public:
    virtual ~GRAY() = default;
    GRAY() = delete;
    /**
     * @brief GRAY
     * @param v
     */
    explicit inline GRAY(double v) : Color(), value(v) {}

 public:
    RGB rgb() const override;
    ERGB ergb() const override;
    HSL hsl() const override;
    HSV hsv() const override;
    HWB hwb() const override;
    CMYK cmyk() const override;
    XYZ xyz() const override;
    LAB lab() const override;
    LCH lch() const override;
    ANSI16 ansi16() const override;
    ANSI256 ansi256() const override;
    HCG hcg() const override;
    GRAY gray() const override;
    CSS css() const override;

 public:
    /**
     * @brief value
     */
    double value;
};

}  // namespace color
}  // namespace rili
