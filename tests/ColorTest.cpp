#include <rili/Color.hpp>
#include <rili/Test.hpp>

TEST(RGB, HSL) {
    rili::color::HSL c1 = rili::color::RGB(140, 200, 100).hsl();
    rili::color::HSL c2(96, 48, 59);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
}

TEST(RGB, HSV) {
    rili::color::HSV c1 = rili::color::RGB(140, 200, 100).hsv();
    rili::color::HSV c2(96, 50, 78);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.value, c2.value, 1.0);
}

TEST(RGB, HWB) {
    rili::color::HWB c1 = rili::color::RGB(140, 200, 100).hwb();
    rili::color::HWB c2(96, 39, 22);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
    EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
}

TEST(RGB, CMYK) {
    {
        rili::color::CMYK c1 = rili::color::RGB(140, 200, 100).cmyk();
        rili::color::CMYK c2(30, 0, 50, 22);
        EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
        EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
        EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
        EXPECT_NEAR(c1.key, c2.key, 1.0);
    }
    {
        rili::color::CMYK c1 = rili::color::RGB(0, 0, 0).cmyk();
        rili::color::CMYK c2(0, 0, 0, 100);
        EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
        EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
        EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
        EXPECT_NEAR(c1.key, c2.key, 1.0);
    }
    {
        rili::color::CMYK c1 = rili::color::RGB(0, 0, 1).cmyk();
        rili::color::CMYK c2(100, 100, 0, 100);
        EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
        EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
        EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
        EXPECT_NEAR(c1.key, c2.key, 1.0);
    }
}

TEST(RGB, XYZ) {
    rili::color::XYZ c1 = rili::color::RGB(92, 191, 84).xyz();
    rili::color::XYZ c2(25, 40, 15);
    EXPECT_NEAR(c1.x, c2.x, 1.0);
    EXPECT_NEAR(c1.y, c2.y, 1.0);
    EXPECT_NEAR(c1.z, c2.z, 1.0);
}

TEST(RGB, LAB) {
    rili::color::LAB c1 = rili::color::RGB(92, 191, 84).lab();
    rili::color::LAB c2(70, -50, 45);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.a, c2.a, 1.0);
    EXPECT_NEAR(c1.b, c2.b, 1.0);
}

TEST(RGB, LCH) {
    rili::color::LCH c1 = rili::color::RGB(92, 191, 84).lch();
    rili::color::LCH c2(70, 67, 138);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.chroma, c2.chroma, 1.0);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
}

TEST(RGB, ANSI16) {
    rili::color::ANSI16 c1 = rili::color::RGB(92, 191, 84).ansi16();
    rili::color::ANSI16 c2(2);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(RGB, ANSI256) {
    rili::color::ANSI256 c1 = rili::color::RGB(92, 191, 84).ansi256();
    rili::color::ANSI256 c2(114);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(RGB, GRAY) {
    {
        rili::color::GRAY c1 = rili::color::RGB(0, 0, 0).gray();
        rili::color::GRAY c2(0);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
    {
        rili::color::GRAY c1 = rili::color::RGB(128, 128, 128).gray();
        rili::color::GRAY c2(50);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
    {
        rili::color::GRAY c1 = rili::color::RGB(255, 255, 255).gray();
        rili::color::GRAY c2(100);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
    {
        rili::color::GRAY c1 = rili::color::RGB(0, 128, 255).gray();
        rili::color::GRAY c2(50);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
}

TEST(RGB, HCG) {
    rili::color::HCG c1 = rili::color::RGB(140, 200, 100).hcg();
    rili::color::HCG c2(96, 39, 65);
    EXPECT_NEAR(c1.grayness, c2.grayness, 1.0);
    EXPECT_NEAR(c1.chroma, c2.chroma, 1.0);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
}

TEST(HSL, ANSI16) {
    rili::color::ANSI16 c1 = rili::color::HSL(240, 100, 50).ansi16();
    rili::color::ANSI16 c2(12);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(HSL, ANSI256) {
    rili::color::ANSI256 c1 = rili::color::HSL(240, 100, 50).ansi256();
    rili::color::ANSI256 c2(21);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(HSL, CMYK) {
    {
        rili::color::CMYK c1 = rili::color::HSL(96, 48, 59).cmyk();
        rili::color::CMYK c2(30, 0, 50, 21);
        EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
        EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
        EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
        EXPECT_NEAR(c1.key, c2.key, 1.0);
    }
    {
        rili::color::CMYK c1 = rili::color::HSL(0, 0, 0).cmyk();
        rili::color::CMYK c2(0, 0, 0, 100);
        EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
        EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
        EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
        EXPECT_NEAR(c1.key, c2.key, 1.0);
    }
}

TEST(HSL, HCG) {
    rili::color::HCG c1 = rili::color::HSL(96, 48, 59).hcg();
    rili::color::HCG c2(96, 39, 65);
    EXPECT_NEAR(c1.grayness, c2.grayness, 1.0);
    EXPECT_NEAR(c1.chroma, c2.chroma, 1.0);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
}

TEST(HSL, HSV) {
    {
        rili::color::HSV c1 = rili::color::HSL(96, 48, 59).hsv();
        rili::color::HSV c2(96, 50, 79);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
    {
        rili::color::HSV c1 = rili::color::HSL(0, 0, 0).hsv();
        rili::color::HSV c2(0, 0, 0);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
        EXPECT_NEAR(c1.value, c2.value, 1.0);
    }
}

TEST(HSL, HWB) {
    {
        rili::color::HWB c1 = rili::color::HSL(96, 48, 59).hwb();
        rili::color::HWB c2(96, 39, 22);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
        EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
    }
    {
        rili::color::HWB c1 = rili::color::HSL(0, 0, 0).hwb();
        rili::color::HWB c2(0, 0, 100);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
        EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
    }
}

TEST(HSL, RGB) {
    {
        rili::color::RGB c1 = rili::color::HSL(96, 48, 59).rgb();
        rili::color::RGB c2(140, 200, 100);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HSL(0, 0, 0).rgb();
        rili::color::RGB c2(0, 0, 0);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
}

TEST(GRAY, RGB) {
    {
        rili::color::RGB c1 = rili::color::GRAY(0).rgb();
        rili::color::RGB c2(0, 0, 0);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::GRAY(50).rgb();
        rili::color::RGB c2(127, 127, 127);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::GRAY(100).rgb();
        rili::color::RGB c2(255, 255, 255);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
}

TEST(GRAY, HSL) {
    rili::color::HSL c1 = rili::color::GRAY(50).hsl();
    rili::color::HSL c2(0, 0, 50);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
}

TEST(GRAY, HSV) {
    rili::color::HSV c1 = rili::color::GRAY(50).hsv();
    rili::color::HSV c2(0, 0, 50);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.value, c2.value, 1.0);
}

TEST(GRAY, HWB) {
    rili::color::HWB c1 = rili::color::GRAY(50).hwb();
    rili::color::HWB c2(0, 100, 50);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
    EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
}

TEST(GRAY, CMYK) {
    rili::color::CMYK c1 = rili::color::GRAY(50).cmyk();
    rili::color::CMYK c2(0, 0, 0, 50);
    EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
    EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
    EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
    EXPECT_NEAR(c1.key, c2.key, 1.0);
}

TEST(GRAY, LAB) {
    rili::color::LAB c1 = rili::color::GRAY(50).lab();
    rili::color::LAB c2(50, 0, 0);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.a, c2.a, 1.0);
    EXPECT_NEAR(c1.b, c2.b, 1.0);
}

TEST(HSV, RGB) {
    rili::color::RGB c1 = rili::color::HSV(96, 50, 78).rgb();
    rili::color::RGB c2(139, 198, 99);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(HSV, HSL) {
    {
        rili::color::HSL c1 = rili::color::HSV(96, 50, 78).hsl();
        rili::color::HSL c2(96, 47, 59);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
        EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    }
    {
        rili::color::HSL c1 = rili::color::HSV(0, 0, 0).hsl();
        rili::color::HSL c2(0, 0, 0);
        EXPECT_NEAR(c1.hue, c2.hue, 1.0);
        EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
        EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    }
}

TEST(HSV, HWB) {
    rili::color::HWB c1 = rili::color::HSV(96, 50, 78).hwb();
    rili::color::HWB c2(96, 39, 22);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
    EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
}

TEST(HSV, CMYK) {
    rili::color::CMYK c1 = rili::color::HSV(96, 50, 78).cmyk();
    rili::color::CMYK c2(30, 0, 50, 22);
    EXPECT_NEAR(c1.cyan, c2.cyan, 1.0);
    EXPECT_NEAR(c1.magenta, c2.magenta, 1.0);
    EXPECT_NEAR(c1.yellow, c2.yellow, 1.0);
    EXPECT_NEAR(c1.key, c2.key, 1.0);
}

TEST(HSV, ANSI16) {
    rili::color::ANSI16 c1 = rili::color::HSV(240, 100, 100).ansi16();
    rili::color::ANSI16 c2(12);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(HSV, ANSI256) {
    rili::color::ANSI256 c1 = rili::color::HSV(240, 100, 100).ansi256();
    rili::color::ANSI256 c2(21);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(HSV, HCG) {
    rili::color::HCG c1 = rili::color::HSV(96, 50, 78).hcg();
    rili::color::HCG c2(96, 39, 64);
    EXPECT_NEAR(c1.grayness, c2.grayness, 1.0);
    EXPECT_NEAR(c1.chroma, c2.chroma, 1.0);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
}

TEST(CMYK, RGB) {
    rili::color::RGB c1 = rili::color::CMYK(30, 0, 50, 22).rgb();
    rili::color::RGB c2(139, 198, 99);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(CMYK, HSL) {
    rili::color::HSL c1 = rili::color::CMYK(30, 0, 50, 22).hsl();
    rili::color::HSL c2(96, 47, 59);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
}

TEST(CMYK, HSV) {
    rili::color::HSV c1 = rili::color::CMYK(30, 0, 50, 22).hsv();
    rili::color::HSV c2(96, 50, 78);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.value, c2.value, 1.0);
}

TEST(CMYK, HWB) {
    rili::color::HWB c1 = rili::color::CMYK(30, 0, 50, 22).hwb();
    rili::color::HWB c2(96, 39, 22);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.whiteness, c2.whiteness, 1.0);
    EXPECT_NEAR(c1.blackness, c2.blackness, 1.0);
}

TEST(CMYK, ANSI16) {
    rili::color::ANSI16 c1 = rili::color::CMYK(30, 0, 50, 22).ansi16();
    rili::color::ANSI16 c2(3);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(CMYK, ANSI256) {
    rili::color::ANSI256 c1 = rili::color::CMYK(30, 0, 50, 22).ansi256();
    rili::color::ANSI256 c2(150);
    EXPECT_EQ(c1.value, c2.value);
}

TEST(XYZ, RGB) {
    {
        rili::color::RGB c1 = rili::color::XYZ(25, 40, 15).rgb();
        rili::color::RGB c2(97, 189, 85);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::XYZ(50, 100, 100).rgb();
        rili::color::RGB c2(0, 255, 241);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
}

TEST(XYZ, LAB) {
    rili::color::LAB c1 = rili::color::XYZ(25, 40, 15).lab();
    rili::color::LAB c2(69, -48, 44);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.a, c2.a, 1.0);
    EXPECT_NEAR(c1.b, c2.b, 1.0);
}

TEST(XYZ, LCH) {
    rili::color::LCH c1 = rili::color::XYZ(25, 40, 15).lch();
    rili::color::LCH c2(69, 65, 137);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.chroma, c2.chroma, 1.0);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
}

TEST(LAB, XYZ) {
    rili::color::XYZ c1 = rili::color::LAB(69, -48, 44).xyz();
    rili::color::XYZ c2(25, 39, 15);
    EXPECT_NEAR(c1.x, c2.x, 1.0);
    EXPECT_NEAR(c1.y, c2.y, 1.0);
    EXPECT_NEAR(c1.z, c2.z, 1.0);
}

TEST(LAB, RGB) {
    rili::color::RGB c1 = rili::color::LAB(75, 20, -30).rgb();
    rili::color::RGB c2(194, 174, 239);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(LCH, RGB) {
    rili::color::RGB c1 = rili::color::LCH(69, 65, 137).rgb();
    rili::color::RGB c2(97, 188, 83);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(LCH, XYZ) {
    rili::color::XYZ c1 = rili::color::LCH(69, 65, 137).xyz();
    rili::color::XYZ c2(25, 39, 15);
    EXPECT_NEAR(c1.x, c2.x, 1.0);
    EXPECT_NEAR(c1.y, c2.y, 1.0);
    EXPECT_NEAR(c1.z, c2.z, 1.0);
}

TEST(LCH, LAB) {
    rili::color::LAB c1 = rili::color::LCH(69, 65, 137).lab();
    rili::color::LAB c2(69, -48, 44);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
    EXPECT_NEAR(c1.a, c2.a, 1.0);
    EXPECT_NEAR(c1.b, c2.b, 1.0);
}

TEST(ANSI16, RGB) {
    rili::color::RGB c1 = rili::color::ANSI16(11).rgb();
    rili::color::RGB c2(255, 255, 0);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(ANSI256, RGB) {
    {
        rili::color::RGB c1 = rili::color::ANSI256(175).rgb();
        rili::color::RGB c2(204, 102, 153);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::ANSI256(224).rgb();
        rili::color::RGB c2(255, 204, 204);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
}

TEST(ANSI256, RGB_ANSI256) {
    for (int i = 16; i < 256; i++) {
        rili::color::ANSI256 c = rili::color::ANSI256(std::uint8_t(i)).rgb().ansi256();
        if (i == 59) {
            EXPECT_EQ(c.value, 236);
        } else if (i == 102) {
            EXPECT_EQ(c.value, 241);
        } else if (i == 145) {
            EXPECT_EQ(c.value, 246);
        } else if (i == 188) {
            EXPECT_EQ(c.value, 251);
        } else {
            EXPECT_EQ(c.value, static_cast<std::uint8_t>(i));
        }
    }
}

TEST(ANSI16, RGB_ANSI16) {
    for (int i = 0; i < 16; i++) {
        rili::color::ANSI16 c = rili::color::ANSI16(std::uint8_t(i)).rgb().ansi16();
        EXPECT_EQ(c.value, static_cast<std::uint8_t>(i));
    }
}

TEST(HCG, RGB) {
    rili::color::RGB c1 = rili::color::HCG(96, 39, 64).rgb();
    rili::color::RGB c2(139, 199, 99);
    EXPECT_EQ(c1.red, c2.red);
    EXPECT_EQ(c1.green, c2.green);
    EXPECT_EQ(c1.blue, c2.blue);
}

TEST(HCG, HSV) {
    rili::color::HSV c1 = rili::color::HCG(96, 39, 64).hsv();
    rili::color::HSV c2(96, 50, 78);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.value, c2.value, 1.0);
}

TEST(HCG, HSL) {
    rili::color::HSL c1 = rili::color::HCG(96, 39, 64).hsl();
    rili::color::HSL c2(96, 47, 59);
    EXPECT_NEAR(c1.hue, c2.hue, 1.0);
    EXPECT_NEAR(c1.saturation, c2.saturation, 1.0);
    EXPECT_NEAR(c1.lightness, c2.lightness, 1.0);
}

TEST(HWB, RGB) {
    for (int angle = 0; angle <= 360; angle++) {
        {
            rili::color::RGB c1 = rili::color::HWB(angle, 0, 100).rgb();
            rili::color::RGB c2(0, 0, 0);
            EXPECT_EQ(c1.red, c2.red);
            EXPECT_EQ(c1.green, c2.green);
            EXPECT_EQ(c1.blue, c2.blue);
        }
        {
            rili::color::RGB c1 = rili::color::HWB(angle, 100, 0).rgb();
            rili::color::RGB c2(255, 255, 255);
            EXPECT_EQ(c1.red, c2.red);
            EXPECT_EQ(c1.green, c2.green);
            EXPECT_EQ(c1.blue, c2.blue);
        }
        {
            rili::color::RGB c1 = rili::color::HWB(angle, 100, 100).rgb();
            rili::color::RGB c2(127, 127, 127);
            EXPECT_EQ(c1.red, c2.red);
            EXPECT_EQ(c1.green, c2.green);
            EXPECT_EQ(c1.blue, c2.blue);
        }
    }
    {
        rili::color::RGB c1 = rili::color::HWB(0, 0, 0).rgb();
        rili::color::RGB c2(255, 0, 0);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(0, 20, 40).rgb();
        rili::color::RGB c2(153, 51, 51);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(0, 40, 40).rgb();
        rili::color::RGB c2(153, 102, 102);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(0, 40, 20).rgb();
        rili::color::RGB c2(204, 102, 102);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(120, 0, 0).rgb();
        rili::color::RGB c2(0, 255, 0);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(120, 20, 40).rgb();
        rili::color::RGB c2(51, 153, 51);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(120, 40, 40).rgb();
        rili::color::RGB c2(102, 153, 102);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(120, 40, 20).rgb();
        rili::color::RGB c2(102, 204, 102);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(240, 0, 0).rgb();
        rili::color::RGB c2(0, 0, 255);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(240, 20, 40).rgb();
        rili::color::RGB c2(51, 51, 153);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(240, 40, 40).rgb();
        rili::color::RGB c2(102, 102, 153);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
    {
        rili::color::RGB c1 = rili::color::HWB(240, 40, 20).rgb();
        rili::color::RGB c2(102, 102, 204);
        EXPECT_EQ(c1.red, c2.red);
        EXPECT_EQ(c1.green, c2.green);
        EXPECT_EQ(c1.blue, c2.blue);
    }
}
