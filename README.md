# Rili Color

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of multiple color model conversion routines, 
briefly based on: [color-convert](https://github.com/Qix-/color-convert)

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/color)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/color/badges/master/build.svg)](https://gitlab.com/rilis/rili/color/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/color/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/color/commits/master)
